# vim-hybrid-dark-flashy

A dark and flashy colour scheme for (neo)vim, using primary x11 colors.
Based on and inspired by the vim-hybrid-material theme:
https://github.com/kristijanhusak/vim-hybrid-material

